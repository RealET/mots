# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- HTML5: Retrait des `CDATA` et `text/javascript` dans les balises `<script>`
